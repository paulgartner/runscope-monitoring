//
// Generates a PropertyTree JWT token
// sets the variable {{JwtToken}} to the generated token
// Requires: companyKey, propertyTreeApiKey, salt
// Dependencies: CryptoJS
//

function JwtService() {
    "use strict";

    this.companyKey = "<ENTER COMPANY KEY>";
    this.propertyTreeApiKey = "<ENTER PROPERTYTREE API KEY>";
    this.salt = "<ENTER SHARED SECRETE/SALT>";

    this.createJwt = function () {
        var unixTimestamp = parseInt(Date.now() / 1000);
        var combinedKey = this.companyKey + ":" + this.propertyTreeApiKey;

        var encodedHeader = this.encodeBytes(CryptoJS.enc.Utf8.parse(JSON.stringify(new HeaderToken()))); 
        var encodedCliams = this.encodeBytes(CryptoJS.enc.Utf8.parse(JSON.stringify(new ClaimsToken(unixTimestamp, combinedKey))));

        var encodedSignature = this.generateEncodedSignature(encodedHeader, encodedCliams);
        
        return encodedHeader + "." + encodedCliams + "." + encodedSignature;
    };

    this.encodeBytes = function (input) {
        var encodedInput = CryptoJS.enc.Base64.stringify(input).replace(/\+/g, "-").replace(/\//g, "_");
        
        while(encodedInput.charAt(encodedInput.length - 1) === "=") {
            encodedInput = encodedInput.substring(0, encodedInput.length - 1);
        }

        return encodedInput;
    };

    this.generateEncodedSignature = function (encodedHeader, encodedCliams) {
        var content = CryptoJS.enc.Utf8.parse(encodedHeader + "." + encodedCliams)
        var secret = this.decodeBytes(this.salt);
        var signatureBytes = CryptoJS.HmacSHA256(content, secret);

        return this.encodeBytes(signatureBytes);
    };

    this.decodeBytes = function (input) {
        if (input === null) {
            return [0];
        }

        switch (input.length % 4) {
        case 1:
            throw "Invalid base64 string"
        case 2:
            input += "==";
            break;
        case 3:
            input += "=";
            break;
        default:
            break;
        }

        return CryptoJS.enc.Base64.parse(input.replace(/-/g, "+").replace(/_/g, "/"));
    };
}

function HeaderToken() {
    this.alg = "HS256";
    this.typ = "JWT";
}

function ClaimsToken(unixTimestamp, combinedKey) {
    this.iss = "PropertyTree";
    this.iat = unixTimestamp;
    this.exp = unixTimestamp;
    this.url = "";
    this.APIKey = combinedKey;
}

variables.set("JwtToken", "Bearer " + new JwtService().createJwt());
